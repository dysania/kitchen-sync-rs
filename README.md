# Rust Kitchen Sync
The purpose of this crate is to simply find crates that implement basic functionality that you might expect in the Python Standard Library (which has everything).
One can then simply pull in this crate to get them all.
