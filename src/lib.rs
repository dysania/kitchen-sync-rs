//! # The Rust Kitchen Sink
//!
//! Below are crates that are generally useful, with the goal being quick, one-off "script" writing.
//!
//! ## Goals
//! - **High utility / complexity crates**. This value is subjective, and at the whimsy of the maintainer.
//! An example of a crate that would not match is `tokio`, which provides great amounts of utility, but is also
//! very complex.
//! - **Crates useful for one-off scripts**. Once a project gets larger than a few thousand lines of code, it
//! has likely outlived the usefulness of this crate.
//! - **Opinionated feature flags**. Dependencies already have feature flags set that could be useful.
//!
//! ## Non-Goals
//! - Conservative Size. Optimizing for size isn't a concern; this crate is gonna get big.
//!
//! ## Included Crates
//!
//! | Crate | Purpose |
//! |---|---|
//! | anyhow | Easier error handling |
//! | fastrand | Easy Random number generation |
//! | glob | File globbing and matching |
//! | once_cell | Safe global vars |
//! | rayon | Easy concurrency |
//! | regex | Regular Expressions |
//! | structopt | CLI arg parsing |

pub use anyhow;
pub use fastrand;
pub use glob;
pub use once_cell;
pub use rayon;
pub use regex;
pub use structopt;
